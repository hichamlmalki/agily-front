import {  useLayoutEffect, useState } from "react"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import axios from "axios"
import { useParams } from "react-router-dom"

// const KEY = "AIzaSyDrlm9WxxPclWlink7la0qLklnsFhsyMXY"

const Meteo = (props) => {

    const [week, setWeek] = useState([])
    const { lat, lon } = useParams();


     useLayoutEffect(  () => {
        axios.get(`http://localhost:3001/forecast/${lat}/${lon}`).catch(
            (error) => { 
                console.error("error : ", error)
            }
        ).then( (response) => {
            const current = response.data.current
            
            document.querySelector(".current .date").innerHTML = current.date
            document.querySelector(".current .details .jour").innerHTML = current.jour
            document.querySelector(".current .details .nuit").innerHTML = current.nuit
            document.querySelector(".current .details .pressure").innerHTML = current.pressure
            document.querySelector(".current .details .humidity").innerHTML = current.humidity
            document.querySelector(".current .details .wind_speed").innerHTML =current.wind_speed
            document.querySelector(".current .icone").src = current.icone

            // console.log("weeks ",response.data.next_week)

            setWeek(response.data.next_week)

            // console.log("week : ",week)
        


        }
        )

        const src_img = "https://picsum.photos/2080/1040"
        document.body.style.background = `url(${src_img})`
    }, [lat, lon]) 

    return(
        <div id="forecast">
            <div className="left-side">
            <a href="/"><FontAwesomeIcon  className="return" icon={faArrowLeft} key='return' /> </a>
                <div className="current carte">
                    <div className="today">
                        <img className="icone" alt="metéo icône"/>

                        <span className="date"> Aujourd'hui ... </span>
                    </div>
                    <div className="details">
                    <ul>
                            <li>Jour | <span className="jour"></span>°C</li>
                            <li>Nuit | <span className="nuit"></span>°C</li>
                            <li>Humadité | <span className="humidity"></span>%</li>
                        </ul>
                        <ul>
                            <li>Pression | <span className="pressure"></span> hPa</li>
                            <li>Vent | <span className="wind_speed"></span> Km/h</li>
                        </ul>
                    </div>
                </div>  
            </div>

            <div className="next-week" >{
                week.map((carte) => { return(
                        <div className="dayCard carte" key={carte.jour}>
                            <img src={carte.icone} className="icone" alt="metéo icône"/>
                            <p className="day">
                                {carte.jour}
                                <span>{carte.date}</span>
                            </p>
        
                            <div className="temp"> {carte.temp}°C</div>
        
                        </div>)
                        
                    })
                }

            </div>

        </div>

        
        
    )
}

export default Meteo