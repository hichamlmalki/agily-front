import './App.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import {  useState } from 'react';
import axios from 'axios';
import message  from 'antd-message';
import { useNavigate } from "react-router-dom";


function App() {

  const navigate = useNavigate();
  const [city, changeCity] = useState("")

  const handleCity = (e) => {
    changeCity(e.target.value)
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    await axios("http://localhost:3001/city/"+city)
    .then( response => {
      const coords = response.data.coord
      console.log("coords",coords)
       navigate(`/Forecast/${coords.lat}/${coords.lon}`)
      
    })
    .catch( error => {
      message.error(error.response.data.message, 2000)
    })
    


  }

  return (
    <div className="App">
      <div>
        <img src='./logo.png' id='logo' alt="logo" />
        <form onSubmit={handleSubmit}>

            <input name='city' value={city} onChange={handleCity}
            autoComplete="address-level1" placeholder='Search'/>

            <FontAwesomeIcon className='loop' icon={faMagnifyingGlass} />
        </form>
      </div>
    </div>
  );
}

export default App;
